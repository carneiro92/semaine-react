const INITIAL_STATE = {
    toggle:false
};
function toggleReducer(state = INITIAL_STATE, action){
    switch(action.type){
        case 'SWITCH_TOGGLE':
            return {
                toggle: !state.toggle
            }
        default:
            return state
    }
}
export default toggleReducer;
