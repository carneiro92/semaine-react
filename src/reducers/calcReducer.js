import { calculator } from "../service/calculator";

const INITIAL_STATE = {
  input1: 20,
  input2: 20,
  operation: "x",
  result: 0,
  loader: false,
};
function calcReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case "UPDATE_INPUT_1":
      return {
        ...state,
        input1: action.value,
      };
    case "UPDATE_INPUT_2":
      return {
        ...state,
        input2: action.value,
      };
    case "UPDATE_OPERATION":
      return {
        ...state,
        operation: action.value,
      };
    case "UPDATE_LOADER":
      return {
        ...state,
        loader: action.status,
      };
    case "UPDATE_RESULT":
      return {
        ...state,
        result: calculator(state.input1, state.operation, state.input2),
      };
    default:
      return state;
  }
}
export default calcReducer;
