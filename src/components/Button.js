import React, {Component} from 'react';
import './App.css';


class Button extends Component {
    render() {
        return (
            <button className="App-Button" onClick={this.props.onClick}>{this.props.label}</button>
        );
    }
}
export default Button;