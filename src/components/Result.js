import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './App.css';


class Result extends Component {
    render() {
        return (
            <div>
                <p className="App-P"> Result={this.props.result}</p>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
      result: state.calcReducer.result,
    }
  }
  
  function mapDispatchToProps(dispatch) {
    return bindActionCreators({ }, dispatch)
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(Result)
