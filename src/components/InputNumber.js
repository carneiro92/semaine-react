import React, {Component} from 'react';
import './App.css';


class InputNumber extends Component {
    render() {
        return (
            <input className="App-Input" value={this.props.stateVal} onChange={this.props.onChange}></input>
        );
    }
}
export default InputNumber;