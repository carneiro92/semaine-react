import React from 'react';
import { switchToggle } from "../actions/toggle";
import { updateInput1,
  updateInput2,
  updateOperation,
  updateResult,
  updateInput1WithFetchedRandom } from "../actions/calc";
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import './App.css'
import InputNumber from '../components/InputNumber'
import Operation from '../components/Operation'
import Button from '../components/Button'
import Result from './Result';
class App extends React.Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          {this.props.toggle
            ? 'REACT'
            : 'REDUX'}
          <button onClick={this.props.switchToggle}>TOGGLE</button>
        </header>
        <div>
          <InputNumber onChange={(event) => this.props.updateInput1(event.target.value)} stateVal={this.props.input1} />
          <Operation onChange={(event) => this.props.updateOperation(event.target.value)} />
          <InputNumber onChange={(event) => this.props.updateInput2(event.target.value)}stateVal={this.props.input2} />
        </div>
        <div>
          <Button label='rand' onClick={this.props.updateInput1WithFetchedRandom} />
          <Button label='calc' onClick={this.props.updateResult} />
        </div>
        <div>
          <Result/>
        </div>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    toggle: state.toggleReducer.toggle,
    input1: state.calcReducer.input1,
    input2: state.calcReducer.input2,
    operation: state.calcReducer.operation
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ 
    switchToggle,
    updateInput1,
    updateInput2,
    updateOperation,
    updateInput1WithFetchedRandom,
    updateResult
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
