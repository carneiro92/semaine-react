import React, {Component} from 'react';
import './App.css';


class Operation extends Component {
    render() {
        return (
            <div>
                <select className="App-Select" onChange={this.props.onChange}>
                    <option value='x'>x</option>
                    <option value='+'>+</option>
                    <option value='-'>-</option>
                    <option value='/'>/</option>
                </select>
            </div>
        );
    }
}
export default Operation;