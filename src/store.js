import {applyMiddleware, combineReducers, createStore} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import toggleReducer from './reducers/toggle-reducer';
import calcReducer from "./reducers/calcReducer";
import thunk from 'redux-thunk';

const middleware = [
    thunk
]
const composeEnhancers = composeWithDevTools({
    // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});
let store = createStore(combineReducers({calcReducer, toggleReducer}),
    composeEnhancers(
        applyMiddleware(...middleware)
    )
);
export default store;
