export const updateInput1 = (value) => {
  return {
      type: 'UPDATE_INPUT_1',
      value
  }
}
export const updateInput2 = (value) => {
  return {
      type: 'UPDATE_INPUT_2',
      value
  }
}
export const updateOperation = (value) => {
  return {
      type: 'UPDATE_OPERATION',
      value
  }
}
export const updateLoader = (status) => {
  return {
      type : 'UPDATE_LOADER',
      status
  }
}
export const updateResult = () => {
  return {
      type: 'UPDATE_RESULT'
  }
}
export const updateInput1WithFetchedRandom = () => {
  return async (dispatch) => {
      dispatch(updateLoader(true)); //allows to know if is loading or not (to use with .gif loader)
      //http://www.randomnumberapi.com/api/v1.0/random?min=100&max=1000&count=1
      const response = await fetch('http://www.randomnumberapi.com/api/v1.0/random?min=100&max=1000&count=2');
      const data = await response.json();
      dispatch(updateInput1(data[0]));
      dispatch(updateInput2(data[1]));
      dispatch(updateLoader(false));
  }
}


